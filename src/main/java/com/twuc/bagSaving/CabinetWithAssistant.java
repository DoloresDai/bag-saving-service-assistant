package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class CabinetWithAssistant extends Cabinet {
    private  List<Cabinet> cabinets ;

    public CabinetWithAssistant(List<Cabinet> cabinets, LockerSetting... lockerSettings) {
        super(lockerSettings);
        this.cabinets = cabinets;
    }
    public CabinetWithAssistant(LockerSetting... lockerSettings) {
        super(lockerSettings);
    }

    public List<Cabinet> getCabinets() {
        return cabinets;
    }

    public void setCabinets(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
    }
}
