package com.twuc.bagSaving;

public class BagSavingArgumentAssistant {
    public static Object[][] createCabinetWithOnlyOneEmptyLockerAndSavableSizesWithAssistant() {
        return new Object[][]{
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.BIG, 1)), BagSize.BIG, LockerSize.BIG},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.BIG, 1)), BagSize.MEDIUM, LockerSize.BIG},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.BIG, 1)), BagSize.SMALL, LockerSize.BIG},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.BIG, 1)), BagSize.MINI, LockerSize.BIG},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.MEDIUM, 1)), BagSize.MEDIUM, LockerSize.MEDIUM},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.MEDIUM, 1)), BagSize.SMALL, LockerSize.MEDIUM},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.MEDIUM, 1)), BagSize.MINI, LockerSize.MEDIUM},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.SMALL, 1)), BagSize.SMALL, LockerSize.SMALL},
                new Object[]{new CabinetWithAssistant(LockerSetting.of(LockerSize.SMALL, 1)), BagSize.MINI, LockerSize.SMALL}
        };
    }
}
