package com.twuc.bagSaving;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

class SaveAndGetBagWithAssistantTest extends BagSavingArgumentAssistant {

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgumentAssistant#createCabinetWithOnlyOneEmptyLockerAndSavableSizesWithAssistant")
    void should_get_a_ticket_when_saving_a_bag(CabinetWithAssistant cabinet, BagSize bagSize, LockerSize lockerSize) {
        Ticket ticket = cabinet.save(new Bag(bagSize), lockerSize);
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgumentAssistant#createCabinetWithOnlyOneEmptyLockerAndSavableSizesWithAssistant")
    void should_save_and_get_bag_when_locker_is_empty(CabinetWithAssistant cabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        Ticket ticket = cabinet.save(savedBag, lockerSize);
        Bag fetchedBag = cabinet.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_bags_according_to_arranged_order(Cabinet cabinet,BagSize bagSize) {

    }
}
